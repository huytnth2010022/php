$(document).ready(function() {


    const inputStreetName = $('input[name=streetname]');
    const inputDistrict = $('input[name=district]');
    const inputFounding = $('input[name=founding]');
    const validationRule = /^[aA-zZ\s]+$/;
    const inputDescription = $('textarea[name=description]');
    const inputStatus = $('input[name=status]');
    $('form[name=street]').submit(function(event) {

        if (inputStreetName.val() === "") {
            event.preventDefault();
            $('#error_founding').empty();
            $('#error_district').empty();
            $('#error_name').html("Error");

        } else {
            if (inputFounding.val() === "") {
                event.preventDefault();
                $('#error_name').empty();
                $('#error_founding').html("Error");
                $('#error_district').empty();
            } else {
                if (inputDistrict.val() === "") {
                    event.preventDefault();
                    $('#error_name').empty();
                    $('#error_district').html("Error");
                    $('#error_founding').empty();
                } else {
                    event.preventDefault();
                    let data = {
                        street_name: inputStreetName.val(),
                        district: inputDistrict.val(),
                        description: inputDescription.val(),
                        status: inputStatus.val(),
                        date: inputFounding.val(),
                    }

                    $.ajax({
                        url: "http://localhost/assigment/city_management/add_city_json.php",
                        method: "POST",
                        data: JSON.stringify(data),
                        success: function(responseData) {
                            alert(responseData.message);

                        },
                        error: function() {

                            alert("Must handle error.");
                        }
                    });
                }
            }

        }
    });
});