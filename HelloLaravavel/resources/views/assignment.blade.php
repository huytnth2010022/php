<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<meta charset="UTF-8">
<title>Product Form</title>
<link rel="stylesheet" href="/asset/assignment.css">
<meta name="viewport" content="witdth=device-width, initial-scale=1.0">
</head>
<body>
    <div class="table">
<a href="/table">Table</a>
    </div>
<div class="container">
<div class="title">Apartment Form</div>
<form action="/assignment" method="post" name="Apartment-form">
@csrf
<div class="user-details">
@if ($errors->any())

@foreach ($errors->all() as $error)
<div class="input-box-error">
{{$error}}
</div>
@endforeach

@endif
<div class="input-box">
<label for="product-name" class="details">  Apartment Name:</label>
<input type="text" name="name" placeholder="Enter Apartment Name">
</div>

<div class="input-box">
<label for="product-price" class="details">  Price:</label>
<input type="text" name="price" placeholder="Enter Price" >
</div>

<div class="input-box">
<label for="product-description" class="details"> Address:</label>
<input type="text" name="address" placeholder="Enter Address" >
</div>

<div class="input-box">
    <label for="summerote" class="details">  General Information:</label>
    <textarea name="g_infor" id="summerote"  cols="50" rows="5"></textarea>
    </div>

    <div class="input-box">
        <label for="summerote" class="details">  Private Information:</label>
        <textarea name="p_infor" id="summerote"  cols="50" rows="5"></textarea>
        </div>

        <div class="input-box">
            <label for="summerote" class="details">  Image URL:</label>
            <textarea name="image" id="summerote"  cols="50" rows="5"></textarea>
            </div>

            <div class="input-box">
                <label for="product-description" class="details"> Status:</label>
                <input type="number" name="status"  >
                </div>

<div class="card-footer">
<button type="submit" class="submit">Submit</button>
</div>
</div>
</form>
</div>
</body>
</html>
