<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssignmentModel extends Model
{
    protected $fillable =['name','g_infor','address','price','p_infor','image','status'];
    use HasFactory;
}
