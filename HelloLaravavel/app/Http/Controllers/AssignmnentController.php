<?php

namespace App\Http\Controllers;

use App\Http\Requests\AssignmentRequest;
use App\Models\AssignmentModel;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AssignmnentController extends Controller
{
   public function Addapartment()
   {
       return view('assignment');
   }
   public function StoreApartment(AssignmentRequest $request)
   {
       $request->request->remove('_token');
       $ap = new AssignmentModel($request->all());
       $ap->timestamps=false;
       $ap->save();
       return 'success';
   }
   public function tableApartment(){
       return view('table_assignment');
   }
   public function getApartments(Request $request){
      $ap = new AssignmentModel($request->all());
     View::share('table_assignment',$ap);
   }
}
