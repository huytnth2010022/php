<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssignmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:50|min:10',
            'address' => 'required',
            'image'=>'url',
            'price'=>'numeric'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Vui lòng nhập tên.',
            'name.min' => 'Tên phải lớn hơn 10 ký tự.',
            'name.max' => 'Tên phải nhỏ hơn 15 ký tự.',
            'address.required' => 'Vui lòng nhập địa chỉ.',
            'price.numeric'=>'Phải là số',
        ];
    }

    // validate theo business riêng.
    public function withValidator($validator)
    {

        $validator->after(function ($validator) {
            if($this->get('price')<0){
                $validator->errors()->add('price', ' phải lớn hơn 0.');
            }
            switch ($this->get('status')){
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                default:
                    $validator->errors()->add('status', 'status: có giá trị 0, 1, 2, 3.');
                    break;
            }
        });
    }

}
