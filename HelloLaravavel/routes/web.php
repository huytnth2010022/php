<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('form');
});
Route::get('/assignment',[\App\Http\Controllers\AssignmnentController::class,'Addapartment']);
Route::post('/assignment',[\App\Http\Controllers\AssignmnentController::class,'StoreApartment']);

Route::get('/table',[\App\Http\Controllers\AssignmnentController::class,'tableApartment']);
Route::get('/table',[\App\Http\Controllers\AssignmnentController::class,'getApartments']);
