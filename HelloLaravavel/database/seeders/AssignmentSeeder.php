<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AssignmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        \Illuminate\Support\Facades\DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        \Illuminate\Support\Facades\DB::table('assignment_models')->truncate();
        \Illuminate\Support\Facades\DB::table('assignment_models')->insert([
            [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ],
            [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ], [
                'name' => $faker->name,
                'address' =>$faker->address,
                'price' => 1000000,
                'g_infor'=>$faker->sentence,
                'p_infor'=>$faker->sentence,
                'image'=>$faker->image
            ],
        ]);
        \Illuminate\Support\Facades\DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
