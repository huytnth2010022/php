<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignment_models', function (Blueprint $table) {
            $table->id();
         $table->string("name");
         $table->string("address");
         $table->double("price");
         $table->text("g_infor");
         $table->text("p_infor");
         $table->text("image");
         $table->integer("status")->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignment_models');
    }
}
